
class Controller{
	
	public static void main(String[] args) {
			Army a1 = new Army("Person1", 5, 100);
			Terrorist t1 = new Terrorist(4, 100);
			Civilian c1 = new Civilian(1, 50);
			//a1.Display();
			while(true){
			System.out.println("Press 1 to attack Terrorist\nPress 2 to attack Civilian");
			System.out.println("Press 3 to attack Armymen by Terrorist\nPress 4 to attack Armymen by Civilian");
			System.out.println("Press 5 to break game");
			Scanner sc = new Scanner(System.in);
			int choice = sc.nextInt();
		
			if(choice == 1){
				a1.attack(t1);
				t1.getHealth();
			}
			if(choice == 2){
				a1.attack(c1);
				c1.getHealth();
			}
			if(choice == 3){
				t1.attack(a1);
				a1.getHealth();
			}
			if(choice == 4){
				c1.attack(a1);
				a1.getHealth();
			}
			if (choice == 5) {
				break;
			}
		}

	}
	
}