import java.util.*;

class Army{
	String name;
	int attack;
	int health;

	Army(String name, int attack, int health){
		this.name = name;
		this.attack = attack;
		this.health = health;
	}
	public void Display()
	{
		System.out.println("The name is:" + this.name + "\nThe health is:" + this.health + 
			"\nThe attack strength is:" + this.attack);
	}
	public void getHealth()
	{
		System.out.println("The health of Armymen is:" + this.health);
	}
	public void attack(Terrorist t){
		if (t.health > 5) {
			t.health = t.health - this.attack;
		}
		else{
			System.out.println("Enemy Defeated");
		}
	}
	public void attack(Civilian c){
		if (c.health > 5) {
			c.health = c.health - this.attack;
		}
		else{
			System.out.println("Enemy Defeated");
		}
	}

}



class Terrorist extends Enemy{

	Terrorist(int attack,int health){
		super(attack, health, true);
	}
	
}

class Civilian extends Enemy{

	Civilian(int attack,int health){
		super(attack, health, false);
	}

}



